﻿using System;

namespace exercise_13
{
    class Program
    {
        static void Main(string[] args)
        {
           
            Console.Clear();

            var name = "Cullen";
            Console.WriteLine($"{name}");
            
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();
        }
    }
}
